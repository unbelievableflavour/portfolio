import React from "react";
import { useTranslation } from "react-i18next";

function Footer() {
  const { t } = useTranslation();
  return (
	<footer className="footer-section text-center">
		<div className="container">
			<h2 className="section-title mb-5">{t('footer.work_together')}</h2>
			<a href="mailto:bartzaalberg@gmail.com" className="site-btn">{t('footer.get_in_touch')}</a>
			<div className="social-links">
				<a href="https://www.linkedin.com/in/bart-zaalberg-b3630b92/"><span className="fab fa-linkedin"/></a>
				<a href="tel:0658713360"><span className="fa fa-phone"/></a>
				<a href="https://brothersonline.gitlab.io/website/"><span className="fa fa-globe"/></a>
			</div>
			<div className="copyright" dangerouslySetInnerHTML={{__html: t('footer.copyright')}} />
		</div>
	</footer>
  );
}

export default Footer;
