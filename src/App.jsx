import React, { Suspense, useState } from "react";
import './Components/i18n';
import i18next from 'i18next';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import About from "./Views/About";
import Home from "./Views/Home";
import Header from "./Components/Header"
import Footer from "./Components/Footer"

function App() {
  const [language, setLanguage] = useState('nl');

  const toggleLanguage = () => {
    const newLanguage = language === 'nl' ? 'en' : 'nl';
    setLanguage(newLanguage);
    i18next.changeLanguage(newLanguage);
  }

  return (
    <div className="App">
      <Suspense fallback={<div>Loading...</div>}>
        <Router basename={process.env.PUBLIC_URL}>
          <Header toggleLanguage={toggleLanguage} language={language} />
          <Switch>
            <Route path="/about">
              <About />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
          <Footer />
        </Router>
      </Suspense>
    </div>
  );
}

export default App;
