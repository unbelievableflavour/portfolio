import React, {useState} from "react";
import './Components/i18n';
import ReactModal from 'react-modal';
import {useTranslation} from "react-i18next";

const getClassByIndex = (index) => {
  if(index === 1){
    return "col-lg-12";
  }

  if(index === 2 || index === 3){
    return "col-lg-6";
  }

  return "col-lg-4";
}

function GalleryImage({itemIndex, item}) {
  const { t } = useTranslation();
  const [modalIsOpen, setModalIsOpen] = useState(false);

  const openModal = () => {
    setModalIsOpen(true);
  };

  return (
    <div className={`mix col-md-6 ${getClassByIndex(itemIndex)}`}>
      <btn onClick={() => openModal(item)} href={'#'} className={`portfolio-item set-bg background-image ${item.backgroundSize === "contain" ? 'background-contain' : ''}`} style={{backgroundImage: "url(" + process.env.PUBLIC_URL + item.imgUrl + ")"}}>
        <div className="pi-inner">
          <h2>{item.title}</h2>
        </div>
      </btn>
      <ReactModal
        isOpen={modalIsOpen}
        contentLabel="Modal"
        style={{
          content: {
            maxWidth: "800px",
            margin: "0 auto"
          }
        }}
          >
          <button className="btn modal-close-btn" onClick={() => setModalIsOpen(false)}>{t('modal.close')}</button>
          <h1>{item.title}</h1>
          <img src={process.env.PUBLIC_URL + item.imgUrl}  alt={item.title} />
          <div className={"info"}>
            <h2>Info</h2>
            <p>{item.description}</p>
            {item.link && <a className="btn website-btn" href={item.link}>{t('modal.website')}</a>}
            {item.reviews && (
              <div className={"reviews"}>
                <h3>Reviews</h3>
                <ul>
              {item.reviews.map(review => (
                <li>
                <a href={review.link}>{review.name}</a>
                </li>
                ))}
                </ul>
              </div>
            )}
          </div>
      </ReactModal>
    </div>
  );
}

export default GalleryImage;
